mod bindings {
    ::windows::include_bindings!();
}

use bindings::windows::win32::system_services as winsys;
use bindings::windows::win32::windows_programming as winprog;

/*
 * コンソールをクリアする
 * 参考: https://docs.microsoft.com/ja-jp/windows/console/clearing-the-screen
 */
pub fn clear_console() {
    // コンソールのハンドラを取得
    let h_console = unsafe { winprog::GetStdHandle(winprog::STD_HANDLE_TYPE::STD_OUTPUT_HANDLE) };

    // 初期化
    let coord_screen = winsys::COORD { x: 0, y: 0 };    // home for the cursor
    let mut c_chars_written : u32 = 0;
    let mut csbi = winsys::CONSOLE_SCREEN_BUFFER_INFO {
        dw_size: winsys::COORD { x: 0, y: 0 },
        dw_cursor_position: winsys::COORD { x: 0, y: 0 },
        w_attributes: 0,
        sr_window: winsys::SMALL_RECT { left: 0, top: 0, right: 0, bottom: 0 },
        dw_maximum_window_size: winsys::COORD { x: 0, y: 0 }
    };

    // コンソールの情報を取得
    if ! unsafe { winsys::GetConsoleScreenBufferInfo(h_console, &mut csbi).as_bool() } {
        return;
    }

    let dw_con_size = (csbi.dw_size.x * csbi.dw_size.y) as u32;
    
    // 画面を空白文字で埋める
    if ! unsafe { winsys::FillConsoleOutputCharacterA(
        h_console,
        ' ' as i8,
        dw_con_size,
        coord_screen.clone(),
        &mut c_chars_written
    ).as_bool() } {
        return;
    }

    if ! unsafe { winsys::GetConsoleScreenBufferInfo(h_console, &mut csbi).as_bool() } {
        return;
    }

    // コンソールの属性を設定
    if ! unsafe { winsys::FillConsoleOutputAttribute(
        h_console,
        csbi.w_attributes,
        dw_con_size,
        coord_screen.clone(),
        &mut c_chars_written
    ).as_bool() } {
        return;
    }

    // カーソルを原点に戻す
    unsafe { winsys::SetConsoleCursorPosition(h_console, coord_screen) };

}