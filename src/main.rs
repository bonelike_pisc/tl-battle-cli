mod windows;
mod log;

use std::rc::Rc;
use tl_battle_core::*;
use tl_battle_core::cmd_skill::*;
use tl_battle_core::state::*;
use tl_battle_core::event::*;
use tl_battle_core::task::*;
use tl_battle_core::ai::*;

fn main() {
    let mut battle = make_battle();
    let mut logger = log::Logger::new();
    logger.init().unwrap();
    let mut count = 0;
    loop {
        let state = battle.take_state().unwrap();
        match state {
            StateEnum::PostInitiate(s) => {
                logger.write("戦闘開始！".to_string()).unwrap();
                battle = s.next();
            },
            StateEnum::PreActionSelect(s) => {
                print_status(&battle);
                logger.tail();
                println!("{}のターン！", s.get_executor().static_ent.name);
                battle = s.next().unwrap();
            },
            StateEnum::CommandSelect(s) => {
                println!("コマンド番号を入力してください");
                let p = s.get_executor();
                p.static_ent.command_skills.iter().enumerate().for_each(|e| {
                    let (i, c) = e;
                    println!("{}: {}", i, c.get_name());
                });
                let buf = read_stdin().unwrap_or(String::new());
                let i = buf.trim().parse().unwrap_or(9999);
                battle = s.select(i).unwrap();
            },
            StateEnum::TargetSelect(s) => {
                println!("コマンドの使用対象の番号を入力してください");
                let executor = s.get_executor();
                let command = s.get_command();
                let participants = battle.get_participants();
                let selectable = command.get_target_type()
                    .get_selectable_participants(executor, &participants);
                selectable.iter().enumerate().for_each(|e| {
                    let (i, p) = e;
                    println!("{}: {}", i, p.static_ent.name);
                });
                let buf = read_stdin().unwrap_or(String::new());
                let i = buf.trim().parse().unwrap_or(9999);
                battle = s.select(i).unwrap();
            },
            StateEnum::TargetConfirm(s) => {
                println!("コマンドの使用対象:");
                let executor = s.get_executor();
                let command = s.get_command();
                let participants = battle.get_participants();
                let selectable = command.get_target_type()
                    .get_selectable_participants(executor.clone(), &participants);
                selectable.iter().for_each(|p| {
                    println!(" {}", p.static_ent.name);
                });
                battle = s.confirm().unwrap();
            },
            StateEnum::PreEventRegister(s) => {
                battle = s.next();
            },
            StateEnum::PostEventRegister(s) => {
                print_status(&battle);
                let ev = s.get_event();
                if let EventInfo::Execution(command, executor, _targets) = ev.get_info() {
                    logger.write(format!("{} は {} を唱え始めた！", executor.static_ent.name, command.get_name())).unwrap();
                }
                logger.tail();
                read_stdin();
                battle = s.next();
            },
            StateEnum::PreRestStart(s) => {
                battle = s.next();
            },
            StateEnum::PostRestStart(s) => {
                print_status(&battle);
                logger.write(format!("{} は休み始めた。", s.get_executor().get_name())).unwrap();
                logger.tail();
                battle = s.next();
            }
            StateEnum::PreEventExecute(s) => {
                let ev = s.get_event();
                if let EventInfo::Execution(command, executor, _targets) = ev.get_info() {
                    logger.write(format!("*** {} の {} が発動！ ***", executor.static_ent.name, command.get_name())).unwrap();
                }
                logger.tail();
                battle = s.next();
            },
            StateEnum::PostEventExecute(s) => {
                print_status(&battle);
                let log = s.get_log();
                for l in log.get() {
                    if let EventLogElement::Damage(p, n) = l {
                        logger.write(format!("*** {} に {}% のダメージ！ ***", p.static_ent.name, to_deci_string(*n))).unwrap();
                    }
                    if let EventLogElement::Recover(p, n) = l {
                        logger.write(format!("*** {} のHPが {}% 回復！ ***", p.static_ent.name, to_deci_string(*n))).unwrap();
                    }
                    if let EventLogElement::SkipTarget(p) = l {
                        logger.write(format!("*** {} には効果がない... ***", p.get_name())).unwrap();
                    }
                }
                logger.tail();
                read_stdin();
                battle = s.next();
            },
            StateEnum::PreKnockdown(s) => {
                battle = s.next();
            },
            StateEnum::PostKnockdown(s) => {
                print_status(&battle);
                let target = s.get_target();
                logger.write(format!("*** {} は倒れた！ ***", target.static_ent.name)).unwrap();
                logger.tail();
                read_stdin();
                battle = s.next();
            },
            StateEnum::PreTurnIncrement(s) => {
                print_status(&battle);
                logger.tail();
                std::thread::sleep(std::time::Duration::from_secs(1));
                battle = s.next();
                count += 1;
                //println!("{}ターン目", count);
            },
            StateEnum::PostTurnIncrement(s) => {
                battle = s.next();
            },
            StateEnum::PreEnd(s) => {
                print_status(&battle);
                logger.write("戦闘終了".to_string());
                logger.tail();
                battle = s.next();
            },
            StateEnum::PostEnd(_s) => {
                break;
            }
        }
    }
}

fn make_battle() -> BattleRunner {
    let attack_skill1 = Rc::new(AttackStandard::new(
        "ショック".to_string(), 6, 150
    ));
    let attack_skill2 = Rc::new(AttackStandard::new(
        "ファイアボール".to_string(), 8, 210
    ));
    let attack_skill3 = Rc::new(AttackStandard::enemy_all(
        "ストリーム".to_string(), 10, 150
    ));
    let recover_skill = Rc::new(RecoverConstant::friend1(
        "ヒール".to_string(), 10, 300
    ));
    let rest = Rc::new(Rest::new(30));
    let p1 = ParticipantStaticEnt {
        name: "火の魔法使い".to_string(),
        group_id: 1,
        is_playable: true,
        ai_pattern: None,
        level: 5,
        power: 6,
        defence: 5,
        endurance: 4,
        speed: 5,
        command_skills: vec![attack_skill2.clone(), rest.clone()],
        passive_skills: Vec::new(),
    };
    let p2 = ParticipantStaticEnt {
        name: "水の魔法使い".to_string(),
        group_id: 1,
        is_playable: true,
        ai_pattern: Some(Box::new(EvenAI)),
        level: 5,
        power: 4,
        defence: 5,
        endurance: 7,
        speed: 4,
        command_skills: vec![attack_skill3.clone(), recover_skill.clone()],
        passive_skills: Vec::new(),
    };
    let p3 = ParticipantStaticEnt {
        name: "雷の魔法使い".to_string(),
        group_id: 1,
        is_playable: true,
        ai_pattern: Some(Box::new(EvenAI)),
        level: 5,
        power: 5,
        defence: 4,
        endurance: 3,
        speed: 8,
        command_skills: vec![attack_skill1.clone(), rest.clone()],
        passive_skills: Vec::new(),
    };
    let e1 = ParticipantStaticEnt {
        name: "敵1".to_string(),
        group_id: 2,
        is_playable: false,
        ai_pattern: Some(Box::new(EvenAI)),
        level: 3,
        power: 5,
        defence: 4,
        endurance: 4,
        speed: 5,
        command_skills: vec![attack_skill2.clone()],
        passive_skills: Vec::new(),
    };
    let e2 = ParticipantStaticEnt {
        name: "敵2".to_string(),
        group_id: 2,
        is_playable: false,
        ai_pattern: Some(Box::new(EvenAI)),
        level: 3,
        power: 3,
        defence: 5,
        endurance: 6,
        speed: 4,
        command_skills: vec![attack_skill3.clone(), recover_skill.clone()],
        passive_skills: Vec::new(),
    };
    let e3 = ParticipantStaticEnt {
        name: "敵3".to_string(),
        group_id: 2,
        is_playable: false,
        ai_pattern: Some(Box::new(EvenAI)),
        level: 3,
        power: 4,
        defence: 3,
        endurance: 3,
        speed: 7,
        command_skills: vec![attack_skill1.clone()],
        passive_skills: Vec::new(),
    };

    BattleRunner::new(vec![p1, p2, p3, e1, e2, e3])
}

fn print_status(battle: &BattleRunner) {
    crate::windows::clear_console();
    println!("==============================");
    for p in battle.get_participants() {
        println!("{} HP:{}% SP:{}%", p.static_ent.name, to_deci_string(p.get_hp()), to_deci_string(p.get_sp()));

        let mut lc = 0;
        for task in p.dynamic_ent.tasks.borrow().get_tasks() {
            if let TaskInfo::ExecutionPrepare(event, cost) = task.get_info() {
                if let EventInfo::Execution(cmd, subject, _target) = event.get_info() {
                    let totoal_cost = cmd.get_cost() * subject.static_ent.power;
                    println!("{} 残コスト: {} / {}", cmd.get_name(), cost, totoal_cost);
                    lc += 1;
                }    
            }
            if let TaskInfo::Rest(_subject, remain, _recovery_rate) = task.get_info() {
                println!("休憩中 あと{}ターン", remain);
                lc += 1;
            }
        }
        if lc == 0 {
            println!("タスクなし");
        }
    }
    println!("==============================");
}

fn read_stdin() -> std::io::Result<String> {
    let mut buffer = String::new();
    std::io::stdin().read_line(&mut buffer)?;
    Ok(buffer)
}

fn to_deci_string(value: u16) -> String {
    format!("{}.{}", value / 10, value % 10)
}
