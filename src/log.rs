use std::io::Write;
use std::collections::VecDeque;

const CACHE_LINE_COUNT: usize = 5;

pub struct Logger {
    log_file_path: String,
    lines_cache: VecDeque<String>,
}

impl Logger {
    pub fn new() -> Self {
        Self {
            log_file_path: "log/tl-battle-cli.log".to_string(),
            lines_cache: VecDeque::new(),
        }
    }

    //　ログファイルを初期化
    pub fn init(&self) -> std::io::Result<()> {
        let mut file = std::fs::OpenOptions::new().create(true).write(true).truncate(true).open(&self.log_file_path)?;
        Ok(())
    }

    pub fn write(&mut self, msg: String) -> std::io::Result<()> {
        //　ファイルに出力
        let mut line = msg.clone();
        line.push('\r');
        line.push('\n');
        let mut file = std::fs::OpenOptions::new().create(true).append(true).open(&self.log_file_path)?;
        file.write_all(line.as_bytes())?;
    
        // キャッシュの世代ローテーションして末尾に追加
        if self.lines_cache.len() >= CACHE_LINE_COUNT {
            let _old_line = self.lines_cache.pop_front();
        }
        self.lines_cache.push_back(msg);
        Ok(())
    }

    pub fn tail(&self) {
        println!("==============================");
        for line in &self.lines_cache {    
            println!("{}", line);
        }
        println!("==============================");
    }
}
