//use bindings::widnows;

fn main() {
    windows::build!(
        windows::win32::system_services::{
            FillConsoleOutputCharacterA,
            FillConsoleOutputAttribute,
            GetConsoleScreenBufferInfo,
            SetConsoleCursorPosition,
        },
        windows::win32::windows_programming::GetStdHandle,
    );
}
